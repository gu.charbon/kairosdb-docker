ARG KAIROS_TAG_OR_BRANCH=v1.2.1
ARG KAIROS_HTTP_PORT=8080
ARG KAIROS_HTTP_ADRESS=0.0.0.0
ARG KAIROS_TELNET_PORT=4242
ARG KAIROS_TELNET_ADRESS=0.0.0.0
ARG KAIROS_PREFIX=/opt
# See https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact

FROM openjdk:8-jdk-alpine AS build

LABEL maintainer="gu.charbon@gmail.com"

ARG KAIROS_TAG_OR_BRANCH

RUN apk add --update libssl1.0 ca-certificates git && \
    rm -rf /var/cache/apk/* && \
    git clone --single-branch -b "${KAIROS_TAG_OR_BRANCH}" https://github.com/kairosdb/kairosdb.git && \
    cd ./kairosdb/ && \
    export CLASSPATH=tools/tablesaw-1.2.6.jar && \
    java make package && \
    mkdir -p /build && \
    tar -xzf ./build/kairosdb*.tar.gz -C /build && \
    cd .. && \
    apk del git && \
    rm -Rf ./kairosdb/

FROM openjdk:8-jre-alpine AS runtime

ARG KAIROSDB_TAG
ARG KAIROS_HTTP_PORT
ARG KAIROS_HTTP_ADRESS
ARG KAIROS_TELNET_PORT
ARG KAIROS_TELNET_ADRESS
ARG KAIROS_PREFIX

ENV KAIROS_HOME="${KAIROS_PREFIX}/kairosdb"

COPY --from=0 /build/kairosdb "${KAIROS_HOME}"

RUN apk add --no-cache bash && \
    sed -i "s/^kairosdb.jetty.port.*$/kairosdb.jetty.port=${KAIROS_HTTP_PORT}/" ${KAIROS_HOME}/conf/kairosdb.properties && \
    sed -i "s/^kairosdb.telnetserver.port.*$/kairosdb.telnetserver.port=${KAIROS_TELNET_PORT}/" ${KAIROS_HOME}/conf/kairosdb.properties && \
    sed -i "s/^kairosdb.jetty.address.*$/kairosdb.jetty.address=${KAIROS_HTTP_ADRESS}/" ${KAIROS_HOME}/conf/kairosdb.properties && \
    sed -i "s/^kairosdb.telnetserver.address.*$/kairosdb.telnetserver.address=${KAIROS_TELNET_ADRESS}/" ${KAIROS_HOME}/conf/kairosdb.properties && \
    echo "Setting work directory in ${KAIROS_HOME}"

WORKDIR ${KAIROS_HOME}

VOLUME ${KAIROS_HOME}
VOLUME /data

EXPOSE ${KAIROSDB_HTTP_PORT} ${KAIROSDB_TELNET_PORT}

CMD /bin/bash bin/kairosdb.sh run
