#!/bin/sh

REPOSITORY=${DOCKER_REPOSITORY:-"gcharbon"}
IMAGE=${DOCKER_IMAGE:-"kairosdb"}
TAG=${TAG:-"latest"}

H2_IMAGE="${REPOSITORY}/${IMAGE}:h2-${TAG}"
CASSANDRA_IMAGE="${REPOSITORY}/${IMAGE}:cassandra-${TAG}"


echo -e "\nBuilding docker image with command:\n'docker build -t ${H2_IMAGE} -f Dockerfile .'\n"

docker build -t ${H2_IMAGE} -f Dockerfile .
docker push ${H2_IMAGE}

echo -e "\nBuilding docker image with command:\n'docker build -t ${CASSANDRA_IMAGE} -f Dockerfile-cassandra'\n"

docker build -t ${CASSANDRA_IMAGE} -f Dockerfile-cassandra .
docker push ${CASSANDRA_IMAGE}

echo -e "\nTagging ${LATEST_IMAGE} as ${REPOSITORY}/${IMAGE}:latest and pushing to registry"
docker tag ${LATEST_IMAGE} ${REPOSITORY}/${IMAGE}:${TAG}
docker push ${REPOSITORY}/${IMAGE}:${TAG}
